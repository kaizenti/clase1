package com.kaizen.others;

import java.text.SimpleDateFormat;

public class Miguel
{
    private long totalPimer=0;

    public void numerosPrimos(int numero)
    {
        long inicio = System.currentTimeMillis();
        // 1 - 100 000
        // Evaluar todos los numeros
        for (int i=2; i<=numero; i++)
        {

            int contCeros = 0;
            // Dividir el numero i por todos sus numeros para determinar si es entero o no
            for (int j=1; j<=i; j++)
            {
                int residuo = i % j;
                if (residuo == 0)
                {
                    contCeros++;
                }
            }
            if (contCeros < 3)
            {
                System.out.println(i + " es numero primo");
            }
        }
        long tiempoFinal = System.currentTimeMillis();
        long total = tiempoFinal - inicio;
        totalPimer = total;
        //System.out.println("Se tardo: " + total + " ms");
    }

    public void numerosPrimosOptimo(int numero)
    {
        long inicio = System.currentTimeMillis();
        // 1 - 100 000
        // Evaluar todos los numeros
        for (int i=2; i<=numero; i++)
        {
            boolean primo = true;
            // Dividir el numero i por todos sus numeros para determinar si es entero o no
            for (int j=2; j<i; j++)
            {
                int residuo = i % j;
                if (residuo == 0)
                {
                    primo = false;
                    break;
                }
            }
            if (primo)
            {
                System.out.println(i + " es numero primo");
            }
        }
        long tiempoFinal = System.currentTimeMillis();
        long total = tiempoFinal - inicio;

        System.out.println("Se tardo el primer ciclo : " + totalPimer + " ms");
        System.out.println("Se tardo el segundo ciclo: " + total + " ms");


    }
}
